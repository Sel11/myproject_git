// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "myProject_gitGameMode.generated.h"

UCLASS(minimalapi)
class AmyProject_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AmyProject_gitGameMode();
};



