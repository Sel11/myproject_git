// Copyright Epic Games, Inc. All Rights Reserved.

#include "myProject_gitGameMode.h"
#include "myProject_gitHUD.h"
#include "myProject_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AmyProject_gitGameMode::AmyProject_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AmyProject_gitHUD::StaticClass();
}
