// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "myProject_gitHUD.generated.h"

UCLASS()
class AmyProject_gitHUD : public AHUD
{
	GENERATED_BODY()

public:
	AmyProject_gitHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

